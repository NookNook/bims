# BIMS

Bash Image Manipulation Script is a simple set of bash scripts for draw, load and save image into different format.
The purpose of this repository is to train my skill into bash scripting and demonstrate to my students the power of this language.
